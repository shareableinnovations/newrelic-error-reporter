const newrelic = require('newrelic');

process.on('uncaughtException', handleError);
process.on('unhandledRejection', handleError);

function handleError(error) {
  newrelic.noticeError(error);
}

module.exports = {
  report: handleError,
  agent: newrelic
};
